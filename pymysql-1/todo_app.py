import pymysql
from secrets import host, user, password


def show_tasks(c):
    select_stmt = """
    SELECT id, task from tasks WHERE done != 1 ORDER BY id;
    """
    c.execute(select_stmt)
    print(c.fetchall())


def mark_as_done(c, id):
    pass


def add_new_task(c, task_name):
    pass


db = pymysql.connect(host, user, password, "")

with db.cursor() as c:
    c.execute("SELECT VERSION()")
    version = c.fetchone()
    print(f"Database version: {version[0]}")
db.close()

# connect to database
db = pymysql.connect(host, user, password, 'todo_app')

# our code

create_stmt = """
    CREATE TABLE IF NOT EXISTS tasks (
    id INT NOT NULL AUTO_INCREMENT,
     task TEXT NOT NULL,
     done BOOLEAN
     );
     """
with db.cursor() as c:
    c.execute(create_stmt)
    while True:
        print("""
            Menu:
            1 - show task
            2 - mark task as done
            3 - add new task
            4 - exit aplication
            """)
        option = int(input('Your choice:'))
        if option not in [1, 2, 3, 4]:
            continue
        if option == 1:
            show_tasks(c)
        elif option == 2:
            # TODO: ask for id
            task_id = input(int(id))
            mark_as_done(c, task_id)
        elif option == 3:
            # TODO: ask for name
            task_name = input(task_name)
            add_new_task(c, task_name)
        elif option == 4:
            break


db.close()



