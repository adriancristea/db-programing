import pymysql
from secrets import host, user, password
import datetime


def convert_line_to_values(row):
    values = row.split(",")
    values[0] = datetime.datetime.strptime(values[0], "%Y-%m-%d %H:%M:%S")
    return values


if __name__ == "__main__":
    sql = """
        INSERT INTO bikesharing 
        (tstamp, cnt, temperature, temperature_feels, humidity, wind_speed, 
        weather_code, is_holiday, is_weekend, season) VALUES
        (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """
    db = pymysql.connect(host, user, password, 'default')
    with db.cursor() as c:
        c._defer_warnings = True
        with open("london-bikes.csv") as f:
            for i, line in enumerate(f):
                if i == 0:
                    continue
                value = convert_line_to_values(line)
                c.execute(sql, value)
                if i > 0 and i % 100 == 0:
                    db.commit()
        db.commit()
    db.close()



