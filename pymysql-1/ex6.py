import pymysql

from secrets import host, user, password

def show_tasks(c):
	# print all open tasks and their ids in order of ids
	select_stmt = """
		SELECT id, task from tasks WHERE done != 1 ORDER BY id;
	"""
	c.execute(select_stmt)
	print(c.fetchall())


def mark_as_done(c, id):
	# update the done field in the table for given id
	pass


def add_new_task(c, task_name):
	# insert a new record to the tasks db (with name)
	pass


db = pymysql.connect(host, user, password, "")
# 1. Connect generically to MySQL and create the todo_app database
with db.cursor() as c:
	c._defer_warnings = True
	c.execute("CREATE SCHEMA IF NOT EXISTS todo_app DEFAULT CHARACTER SET utf8;")
db.close()


# 2. Connect to newly created database todo_app
db = pymysql.connect(host, user, password, "todo_app")

# 3. Create tasks table
create_stmt = """
	CREATE TABLE IF NOT EXISTS tasks (
	id INT PRIMARY KEY AUTO_INCREMENT,
	task_name TEXT NOT NULL,
	done BOOLEAN
	);
"""
with db.cursor() as c:
	c.execute(create_stmt)
	# In a loop:
		# ○ ask user what to do using input()
			# show task list
			# mark task as done
			# add new task
			# exit application
	while True:
		print("""
			Menu:
			1 - show task list
			2 - mark task as done
			3 - add new task
			4 - exit application
			""")
		option = int(input('Your choice:'))
		if option not in [1, 2, 3, 4]:
			continue
		if option == 1:
			show_tasks(c)
		elif option == 2:
			# TODO: ask for id
			mark_as_done(c, task_id)
		elif option == 3:
			# TODO: ask for name
			add_new_task(c, task_name)
		elif option == 4:
			break

db.close()
