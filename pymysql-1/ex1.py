import pymysql
from secrets import host, user, password

db = pymysql.connect(host, user, password, "")
with db.cursor() as c:
    c.execute("CREATE DATABASE IF NOT EXISTS todo_app DEFAULT CHARACTER SET utf8;")
db.close()


